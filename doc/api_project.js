define({
  "name": "API REST - Testwork",
  "version": "1.0.0",
  "description": "Documentación de la API testwork",
  "title": "API Documentación - Testwork",
  "url": "https://testworkjdaza.herokuapp.com/api",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2021-05-15T15:33:32.021Z",
    "url": "https://apidocjs.com",
    "version": "0.27.1"
  }
});
