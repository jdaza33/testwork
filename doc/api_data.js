define({ "api": [
  {
    "type": "POST",
    "url": "/courses/create",
    "title": "Crear un curso",
    "name": "createCourse",
    "group": "Courses",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Nombre del curso</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    name: 'NodeJS de cero a experto',\n}",
          "type": "Object"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    success : 1,\n    course: { ... },\n    message: '...'\n}",
          "type": "type"
        }
      ]
    },
    "filename": "src/controllers/course.ctrl.js",
    "groupTitle": "Courses"
  },
  {
    "type": "GET",
    "url": "/courses/list",
    "title": "Listar los cursos",
    "name": "listCourses",
    "group": "Courses",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    success : 1,\n    course: { ... },\n    message: '...'\n}",
          "type": "type"
        }
      ]
    },
    "filename": "src/controllers/course.ctrl.js",
    "groupTitle": "Courses"
  },
  {
    "type": "GET",
    "url": "/courses/more-students",
    "title": "Obtener el curso con más estudiantes",
    "name": "moreStudents",
    "group": "Courses",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    success : 1,\n    course: { ... },\n    message: '...'\n}",
          "type": "type"
        }
      ]
    },
    "filename": "src/controllers/course.ctrl.js",
    "groupTitle": "Courses"
  },
  {
    "type": "PUT",
    "url": "/enrollments/add-student/:id Agregar un",
    "title": "estudiante a una inscripcion",
    "name": "addStudentEnrollment",
    "group": "Enrollments",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>ID del curso</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "studentId",
            "description": "<p>ID del estudiante</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "credits",
            "description": "<p>Creditos del estudiante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    studentId: 123456789,\n    credits: 56\n}",
          "type": "Object"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    success : 1,\n    enrollment: { ... },\n    message: '...'\n}",
          "type": "type"
        }
      ]
    },
    "filename": "src/controllers/enrollment.ctrl.js",
    "groupTitle": "Enrollments"
  },
  {
    "type": "POST",
    "url": "/enrollments/create",
    "title": "Crear una inscripcion",
    "name": "createEnrollment",
    "group": "Enrollments",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "course",
            "description": "<p>ID del curso</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "allowedValues": [
              "{\"student\"",
              "\"credits\"}"
            ],
            "optional": false,
            "field": "students",
            "description": "<p>Datos del estudiante (student: ID del estudiante, credits: Creditos del estudiante)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    course: 123456789,\n    students: [{\n       student: 1234567891,\n       credits: 20\n     }]\n}",
          "type": "Object"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    success : 1,\n    enrollment: { ... },\n    message: '...'\n}",
          "type": "type"
        }
      ]
    },
    "filename": "src/controllers/enrollment.ctrl.js",
    "groupTitle": "Enrollments"
  },
  {
    "type": "POST",
    "url": "/students/create",
    "title": "Crear un estudiante",
    "name": "createStudent",
    "group": "Students",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Nombre del estudiante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    name: 'Juan Luis Guerra',\n}",
          "type": "Object"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    success : 1,\n    student: { ... },\n    message: '...'\n}",
          "type": "type"
        }
      ]
    },
    "filename": "src/controllers/student.ctrl.js",
    "groupTitle": "Students"
  },
  {
    "type": "GET",
    "url": "/students/fifty-credits Obtener los",
    "title": "estudiantes con creditos mayores a 50",
    "name": "fiftyCredits",
    "group": "Students",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    success : 1,\n    students: { ... },\n    message: '...'\n}",
          "type": "type"
        }
      ]
    },
    "filename": "src/controllers/student.ctrl.js",
    "groupTitle": "Students"
  },
  {
    "type": "GET",
    "url": "/students/list",
    "title": "Listar los Estudiantes",
    "name": "listStudents",
    "group": "Students",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    success : 1,\n    students: { ... },\n    message: '...'\n}",
          "type": "type"
        }
      ]
    },
    "filename": "src/controllers/student.ctrl.js",
    "groupTitle": "Students"
  },
  {
    "type": "POST",
    "url": "/users/check-token",
    "title": "Validar un token JWT",
    "name": "checkTokenUser",
    "group": "Users",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token JWT</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userId",
            "description": "<p>ID del usuario</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjYwOWY5ODc2OG...',\n    userId: 123456789\n}",
          "type": "Object"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    success : 1,\n    isAccess: true,\n    message: '...'\n}",
          "type": "type"
        }
      ]
    },
    "filename": "src/controllers/user.ctrl.js",
    "groupTitle": "Users"
  },
  {
    "type": "POST",
    "url": "/users/create",
    "title": "Crear usuario",
    "name": "createUser",
    "group": "Users",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Nombre</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Correo electronico</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Contraseña</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    name : 'Jose',\n    email: 'jose@mail.com',\n    password: '123456'\n}",
          "type": "Object"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    success : 1,\n    user: { ... },\n    message: '...'\n}",
          "type": "type"
        }
      ]
    },
    "filename": "src/controllers/user.ctrl.js",
    "groupTitle": "Users"
  },
  {
    "type": "POST",
    "url": "/users/list",
    "title": "Listar usuarios",
    "name": "listUsers",
    "group": "Users",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": true,
            "field": "filters",
            "description": "<p>Filtros de busqueda</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    success : 1,\n    users: { ... },\n    message: '...'\n}",
          "type": "type"
        }
      ]
    },
    "filename": "src/controllers/user.ctrl.js",
    "groupTitle": "Users"
  },
  {
    "type": "POST",
    "url": "/users/login",
    "title": "Inicio de sesion",
    "name": "loginUser",
    "group": "Users",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Correo electronico</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Contraseña</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    email: 'jose@mail.com',\n    password: '123456'\n}",
          "type": "Object"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    success : 1,\n    user: { ... },\n    message: '...'\n}",
          "type": "type"
        }
      ]
    },
    "filename": "src/controllers/user.ctrl.js",
    "groupTitle": "Users"
  }
] });
