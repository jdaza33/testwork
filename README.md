<!-- PROJECT LOGO -->

<br />

<p  align="center">

<h3  align="center">TESTWORK</h3>

<p  align="center">

Una prueba de trabajo desarrollada por J. Daza.

<br />

<a  href="https://testworkjdaza.herokuapp.com/doc"><strong>Lee la documentación »</strong></a>

<br />

</p>

</p>

<!-- TABLE OF CONTENTS -->

<details  open="open">

<summary>Contenido:</summary>

<ol>

<li>

<a  href="#primeros-pasos">Primeros Pasos</a>

</li>

<li><a  href="#uso">Uso</a></li>
<li><a  href="#pruebas">Pruebas</a></li>

</ol>

</details>

<!-- GETTING STARTED -->

## Primeros Pasos

Para iniciar el proyecto debe tener ya instalado NodeJS, preferiblemente la ultima versión.

### Requisitos

- npm

```sh

npm install npm@latest -g

```

- nodejs

```sh

v14.15.3

```

### Installation

1. Clonar el repositorio

```sh

git clone https://gitlab.com/jdaza33/testwork.git

```

2. Instalar los paquetes NPM

```sh

npm install

```

3. Ejecutar el servicio

```JS

node .

```

<!-- USAGE EXAMPLES -->

## Uso

Solo tiene pocos endpoints habilitados debido a que es una prueba de trabajo, por favor lea la documentación para mayor informacion. [Documentation](https://testworkjdaza.herokuapp.com/doc)

## Pruebas

Antes de iniciar la prueba, debe tener iniciado el servicio en el puerto 3001, dado que el modulo de supertest apunta a dicha dirección.
Para realizar el test solo debe ejecutar el siguiente comando:

```sh

npm test

```
