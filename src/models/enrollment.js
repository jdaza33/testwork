/**
 * @description Esquema de inscripciones
 */

//Modules
const mongoose = require('mongoose')
const Schema = mongoose.Schema

//Schema Students
const StudentsSchema = new Schema({
  student: { type: String, unique: false, required: true, ref: 'Students' },
  credits: { type: Number, unique: false, required: true },
})

//Schema
const EnrollmentSchema = new Schema({
  course: { type: String, unique: true, required: true, ref: 'Courses' },
  students: {
    type: [StudentsSchema],
    unique: false,
    required: true,
  },
})

module.exports = mongoose.model('Enrollments', EnrollmentSchema)
