/**
 * @description Esquema de usuarios
 */

//Modules
const mongoose = require('mongoose')
const Schema = mongoose.Schema

//Schema
const UserSchema = new Schema({
  name: { type: String, unique: false, required: true },
  email: { type: String, unique: true, required: true },
  password: { type: String, unique: false, required: true },
})

module.exports = mongoose.model('Users', UserSchema)
