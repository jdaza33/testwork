/**
 * @description Esquema de cursos
 */

//Modules
const mongoose = require('mongoose')
const Schema = mongoose.Schema

//Schema
const CourseSchema = new Schema({
  name: { type: String, unique: false, required: true },
})

module.exports = mongoose.model('Courses', CourseSchema)
