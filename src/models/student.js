/**
 * @description Esquema de estudiantes
 */

//Modules
const mongoose = require('mongoose')
const Schema = mongoose.Schema

//Schema
const StudentSchema = new Schema({
  name: { type: String, unique: false, required: true },
})

module.exports = mongoose.model('Students', StudentSchema)
