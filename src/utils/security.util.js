/**
 * @description Seguridad de utilidad
 */

//Modules
const bcrypt = require('bcrypt')
const jwt = require('jwt-simple')
const moment = require('moment')

//Constantes
const saltRounds = 10

/**
 * @description Funcion para encriptar un texto
 * @param {String} text
 */
const createHash = (text) => {
  try {
    return bcrypt.hash(text, saltRounds)
  } catch (error) {
    return error
  }
}

/**
 * @description Funcion para comarar un texto con otro ya encriptado (hash)
 * @param {String} hash
 * @param {String} text
 */
const compareHash = (hash, text) => {
  try {
    return bcrypt.compare(text, hash)
  } catch (error) {
    return error
  }
}

/**
 * @description Funcion para crear un token JWT
 * @param {String} userId
 */
const createTokenJwt = (userId) => {
  try {
    let payload = {
      id: userId,
      expMs: moment().set('day', 7).valueOf(),
    }
    return jwt.encode(payload, process.env.SECRET_JWT)
  } catch (error) {
    return error
  }
}

/**
 * @description Funcion para validar si un token es valido o no ha vencido
 * @param {*} token
 * @param {*} userId
 */
const checkTokenJwt = (token, userId) => {
  try {
    let { id, expMs } = jwt.decode(token, process.env.SECRET_JWT)
    let now = moment().valueOf()

    if (id != userId) return false
    if (expMs < now) return false

    return true
  } catch (error) {
    return error
  }
}

module.exports = {
  createHash,
  compareHash,
  createTokenJwt,
  checkTokenJwt,
}
