/**
 * @description Rutas del endpoint
 */

//Modules
const express = require('express')

//Constantes
const router = express.Router()

//Controllers
const userCtrl = require('../controllers/user.ctrl')
const courseCtrl = require('../controllers/course.ctrl')
const studentCtrl = require('../controllers/student.ctrl')
const enrollmentCtrl = require('../controllers/enrollment.ctrl')

/** Users */
router.post('/users/create', userCtrl.createUser)
router.post('/users/list', userCtrl.listUsers)
router.post('/users/login', userCtrl.loginUser)
router.post('/users/check-token', userCtrl.checkTokenUser)

/** Courses */
router.post('/courses/create', courseCtrl.createCourse)
router.get('/courses/list', courseCtrl.listCourses)
router.get('/courses/more-students', courseCtrl.moreStudents)

/** Students */
router.post('/students/create', studentCtrl.createStudent)
router.get('/students/list', studentCtrl.listStudents)
router.get('/students/fifty-credits', studentCtrl.fiftyCredits)

/** Enrollments */
router.post('/enrollments/create', enrollmentCtrl.createEnrollment)
router.put(
  '/enrollments/add-student/:id',
  enrollmentCtrl.addStudentToEnrollment
)

router.use('*', (req, res) => {
  return res.status(404).send({
    success: 0,
    data: null,
    error: {
      status: 404,
      type: 'Resource not found',
      reason: 'Application does not support the requested path',
    },
  })
})

module.exports = router
