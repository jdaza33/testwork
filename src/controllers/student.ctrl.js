/**
 * @description Controlador de estudiantes
 */

//Services
const { create, getFiftyCredits, list } = require('../services/student.srv')

/**
 *
 * @api {POST} /students/create Crear un estudiante
 * @apiName createStudent
 * @apiGroup Students
 * @apiVersion  1.0.0
 *
 *
 * @apiParam  {String} name Nombre del estudiante
 *
 *
 * @apiParamExample  {Object} Request-Example:
 * {
 *     name: 'Juan Luis Guerra',
 * }
 *
 *
 * @apiSuccessExample {type} Success-Response:
 * {
 *     success : 1,
 *     student: { ... },
 *     message: '...'
 * }
 *
 *
 */
const createStudent = async (req, res, next) => {
  try {
    let student = await create(req.body)

    res.status(200).json({
      success: 1,
      student,
      message: 'Estudiante creado con éxito',
    })
  } catch (error) {
    next(error)
  }
}

/**
 *
 * @api {GET} /students/fifty-credits Obtener los
 * estudiantes con creditos mayores a 50
 * @apiName fiftyCredits
 * @apiGroup Students
 * @apiVersion  1.0.0
 *
 *
 * @apiSuccessExample {type} Success-Response:
 * {
 *     success : 1,
 *     students: { ... },
 *     message: '...'
 * }
 *
 *
 */
const fiftyCredits = async (req, res, next) => {
  try {
    let students = await getFiftyCredits()

    res.status(200).json({
      success: 1,
      students,
      message: 'Estudiantes obtenido con éxito',
    })
  } catch (error) {
    next(error)
  }
}

/**
 *
 * @api {GET} /students/list Listar los Estudiantes
 * @apiName listStudents
 * @apiGroup Students
 * @apiVersion  1.0.0
 *
 *
 * @apiSuccessExample {type} Success-Response:
 * {
 *     success : 1,
 *     students: { ... },
 *     message: '...'
 * }
 *
 *
 */
const listStudents = async (req, res, next) => {
  try {
    let students = await list()

    res.status(200).json({
      success: 1,
      students,
      message: 'Estudiantes obtenido con éxito',
    })
  } catch (error) {
    next(error)
  }
}
module.exports = { createStudent, fiftyCredits, listStudents }
