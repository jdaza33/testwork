/**
 * @description Controlador de inscripciones
 */

//Services
const { create, addStudent } = require('../services/enrollment.srv')

/**
 *
 * @api {POST} /enrollments/create Crear una inscripcion
 * @apiName createEnrollment
 * @apiGroup Enrollments
 * @apiVersion  1.0.0
 *
 *
 * @apiParam  {String} course ID del curso
 * @apiParam  {Array={"student","credits"}} students Datos del estudiante
 * (student: ID del estudiante, credits: Creditos del estudiante)
 *
 *
 * @apiParamExample  {Object} Request-Example:
 * {
 *     course: 123456789,
 *     students: [{
 *        student: 1234567891,
 *        credits: 20
 *      }]
 * }
 *
 *
 * @apiSuccessExample {type} Success-Response:
 * {
 *     success : 1,
 *     enrollment: { ... },
 *     message: '...'
 * }
 *
 *
 */
const createEnrollment = async (req, res, next) => {
  try {
    let enrollment = await create(req.body)

    res.status(200).json({
      success: 1,
      enrollment,
      message: 'Inscripcion creada con éxito',
    })
  } catch (error) {
    next(error)
  }
}

/**
 *
 * @api {PUT} /enrollments/add-student/:id Agregar un
 * estudiante a una inscripcion
 * @apiName addStudentEnrollment
 * @apiGroup Enrollments
 * @apiVersion  1.0.0
 *
 *
 * @apiParam  {String} id ID del curso
 * @apiParam  {String} studentId ID del estudiante
 * @apiParam  {String} credits Creditos del estudiante
 *
 *
 * @apiParamExample  {Object} Request-Example:
 * {
 *     studentId: 123456789,
 *     credits: 56
 * }
 *
 *
 * @apiSuccessExample {type} Success-Response:
 * {
 *     success : 1,
 *     enrollment: { ... },
 *     message: '...'
 * }
 *
 *
 */
const addStudentToEnrollment = async (req, res, next) => {
  try {
    let { id: enrollmentId } = req.params
    let { studentId, credits } = req.body

    let enrollment = await addStudent(enrollmentId, studentId, credits)

    res.status(200).json({
      success: 1,
      enrollment,
      message: 'Estudiante agregado a la inscripcion con éxito',
    })
  } catch (error) {
    next(error)
  }
}

module.exports = { createEnrollment, addStudentToEnrollment }
