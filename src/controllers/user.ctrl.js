/**
 * @description Controlador de usuarios
 */

//Services
const { create, list, login, checkToken } = require('../services/user.srv')

/**
 *
 * @api {POST} /users/create Crear usuario
 * @apiName createUser
 * @apiGroup Users
 * @apiVersion  1.0.0
 *
 *
 * @apiParam  {String} name Nombre
 * @apiParam  {String} email Correo electronico
 * @apiParam  {String} password Contraseña
 *
 *
 * @apiParamExample  {Object} Request-Example:
 * {
 *     name : 'Jose',
 *     email: 'jose@mail.com',
 *     password: '123456'
 * }
 *
 *
 * @apiSuccessExample {type} Success-Response:
 * {
 *     success : 1,
 *     user: { ... },
 *     message: '...'
 * }
 *
 *
 */
const createUser = async (req, res, next) => {
  try {
    let user = await create(req.body)

    res.status(200).json({
      success: 1,
      user,
      message: 'Usuario creado con éxito',
    })
  } catch (error) {
    next(error)
  }
}

/**
 *
 * @api {POST} /users/list Listar usuarios
 * @apiName listUsers
 * @apiGroup Users
 * @apiVersion  1.0.0
 *
 *
 * @apiParam  {Object} [filters] Filtros de busqueda
 *
 *
 *
 * @apiSuccessExample {type} Success-Response:
 * {
 *     success : 1,
 *     users: { ... },
 *     message: '...'
 * }
 *
 *
 */
const listUsers = async (req, res, next) => {
  try {
    let filters = req.body
    let users = await list(filters)
    res.status(200).json({
      success: 1,
      users,
      message: 'Usuarios listados con éxito',
    })
  } catch (error) {
    next(error)
  }
}

/**
 *
 * @api {POST} /users/login Inicio de sesion
 * @apiName loginUser
 * @apiGroup Users
 * @apiVersion  1.0.0
 *
 *
 * @apiParam  {String} email Correo electronico
 * @apiParam  {String} password Contraseña
 *
 *
 * @apiParamExample  {Object} Request-Example:
 * {
 *     email: 'jose@mail.com',
 *     password: '123456'
 * }
 *
 *
 * @apiSuccessExample {type} Success-Response:
 * {
 *     success : 1,
 *     user: { ... },
 *     message: '...'
 * }
 *
 *
 */
const loginUser = async (req, res, next) => {
  try {
    let { email, password } = req.body
    let user = await login(email, password)
    res.status(200).json({
      success: 1,
      user,
      message: 'Inicio de sesión éxitoso',
    })
  } catch (error) {
    next(error)
  }
}

/**
 *
 * @api {POST} /users/check-token Validar un token JWT
 * @apiName checkTokenUser
 * @apiGroup Users
 * @apiVersion  1.0.0
 *
 *
 * @apiParam  {String} token Token JWT
 * @apiParam  {String} userId ID del usuario
 *
 *
 * @apiParamExample  {Object} Request-Example:
 * {
 *     token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjYwOWY5ODc2OG...',
 *     userId: 123456789
 * }
 *
 *
 * @apiSuccessExample {type} Success-Response:
 * {
 *     success : 1,
 *     isAccess: true,
 *     message: '...'
 * }
 *
 *
 */
const checkTokenUser = async (req, res, next) => {
  try {
    let { token, userId } = req.body
    let isAccess = await checkToken(token, userId)
    res.status(200).json({
      success: 1,
      isAccess,
      message: 'Token verificado con éxito',
    })
  } catch (error) {
    next(error)
  }
}

module.exports = {
  createUser,
  listUsers,
  loginUser,
  checkTokenUser,
}
