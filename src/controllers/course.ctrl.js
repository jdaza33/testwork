/**
 * @description Controlador de cursos
 */

//Services
const { create, getMoreStudents, list } = require('../services/course.srv')

/**
 *
 * @api {POST} /courses/create Crear un curso
 * @apiName createCourse
 * @apiGroup Courses
 * @apiVersion  1.0.0
 *
 *
 * @apiParam  {String} name Nombre del curso
 *
 *
 * @apiParamExample  {Object} Request-Example:
 * {
 *     name: 'NodeJS de cero a experto',
 * }
 *
 *
 * @apiSuccessExample {type} Success-Response:
 * {
 *     success : 1,
 *     course: { ... },
 *     message: '...'
 * }
 *
 *
 */
const createCourse = async (req, res, next) => {
  try {
    let course = await create(req.body)

    res.status(200).json({
      success: 1,
      course,
      message: 'Curso creado con éxito',
    })
  } catch (error) {
    next(error)
  }
}

/**
 *
 * @api {GET} /courses/more-students Obtener el curso con más estudiantes
 * @apiName moreStudents
 * @apiGroup Courses
 * @apiVersion  1.0.0
 *
 *
 * @apiSuccessExample {type} Success-Response:
 * {
 *     success : 1,
 *     course: { ... },
 *     message: '...'
 * }
 *
 *
 */
const moreStudents = async (req, res, next) => {
  try {
    let course = await getMoreStudents()

    res.status(200).json({
      success: 1,
      course,
      message: 'Curso obtenido con éxito',
    })
  } catch (error) {
    next(error)
  }
}

/**
 *
 * @api {GET} /courses/list Listar los cursos
 * @apiName listCourses
 * @apiGroup Courses
 * @apiVersion  1.0.0
 *
 *
 * @apiSuccessExample {type} Success-Response:
 * {
 *     success : 1,
 *     course: { ... },
 *     message: '...'
 * }
 *
 *
 */
const listCourses = async (req, res, next) => {
  try {
    let courses = await list()

    res.status(200).json({
      success: 1,
      courses,
      message: 'Cursos listados con éxito',
    })
  } catch (error) {
    next(error)
  }
}
module.exports = { createCourse, moreStudents, listCourses }
