/**
 * @description Server
 */

// Modules
const express = require('express')
const cors = require('cors')
const path = require('path')

//Middlewares
const { errorHandler } = require('../middlewares/error.middleware')

const startExpress = () => {
  return new Promise(async (resolve, reject) => {
    try {
      //APP
      const app = express()

      //Middlewares
      app.use(cors())
      app.use(express.json())
      app.set('view engine', 'html')

      //Routes
      const router = require('../routes/router')
      app.use('/api', router)

      //Doc
      app.use(express.static(path.resolve(__dirname, '../../', './doc')))
      app.use('/doc', function (req, res) {
        res.sendFile(path.resolve(__dirname, '../../', './doc/index.html'))
      })

      //Error
      app.use(errorHandler)

      return resolve(app)
    } catch (error) {
      return reject(error)
    }
  })
}

module.exports = { startExpress }
