/**
 * @description Servicio de usuarios
 */

//Models
const User = require('../models/user')

//Utils
const {
  createHash,
  compareHash,
  createTokenJwt,
  checkTokenJwt,
} = require('../utils/security.util')

/**
 *
 * @description Servicio para crear un usuario
 * @param {Object} data
 */
const create = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //Si la contraseña no viene, por defecto es 123456
      data.password = await createHash(data.password || 123456)

      let user = await User.create(data)

      return resolve(user)
    } catch (error) {
      console.log(error)
      return reject(error)
    }
  })
}

/**
 *
 * @description Servicio para listar usuarios
 * @param {Object} filters
 */
const list = (filters = {}) => {
  return new Promise(async (resolve, reject) => {
    try {
      let users = await User.find(filters, { password: 0 }).lean()

      return resolve(users)
    } catch (error) {
      return reject(error)
    }
  })
}

/**
 *
 * @description Servicio para iniciar sesion
 * @param {string} email
 * @param {password} password
 */
const login = (email, password) => {
  return new Promise(async (resolve, reject) => {
    try {
      //Validamos campos requeridos
      if (!email || !password)
        return reject('Email y contraseña son requeridos')

      //Buscamos el usuario
      let user = await User.findOne({ email }).lean()
      user = JSON.parse(JSON.stringify(user))

      if (!user) return reject('El usuario no existe')

      //Comparamos contraseña
      if (!compareHash(password, user.password))
        return reject('La contraseña invalida')

      //Creamos el token JWT
      let token = createTokenJwt(user._id.toString())
      user.token = token

      //Eliminamos el password de la respuesta
      delete user.password

      return resolve(user)
    } catch (error) {
      return reject(error)
    }
  })
}

/**
 *
 * @description Validamos el token JWT
 * @param {String} token
 * @param {String} userId
 */
const checkToken = (token, userId) => {
  return new Promise(async (resolve, reject) => {
    try {
      return resolve(checkTokenJwt(token, userId))
    } catch (error) {
      return reject(error)
    }
  })
}

module.exports = { create, list, login, checkToken }
