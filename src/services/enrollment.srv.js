/**
 * @description Servicio de inscripciones
 */

//Models
const Enrollment = require('../models/enrollment')

/**
 *
 * @description Servicio para crear una inscripcion
 * @param {Object} data
 */
const create = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      const enrollment = await Enrollment.create(data)

      return resolve(enrollment)
    } catch (error) {
      return reject(error)
    }
  })
}

const addStudent = (enrollmentId, student, credits) => {
  return new Promise(async (resolve, reject) => {
    try {
      const enrollment = await Enrollment.findOneAndUpdate(
        enrollmentId,
        {
          $push: { students: { student, credits } },
        },
        { new: true }
      )

      return resolve(enrollment)
    } catch (error) {
      return reject(error)
    }
  })
}

module.exports = { create, addStudent }
