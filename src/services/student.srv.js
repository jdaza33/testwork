/**
 * @description Servicio de estudiantes
 */

//Models
const Student = require('../models/student')
const Enrollment = require('../models/enrollment')

/**
 *
 * @description Servicio para crear un estudiante
 * @param {Object} data
 */
const create = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      const student = await Student.create(data)

      return resolve(student)
    } catch (error) {
      return reject(error)
    }
  })
}

/**
 *
 * @description Servicio para obtener los estudiantes con más de 50 creditos
 */
const getFiftyCredits = () => {
  return new Promise(async (resolve, reject) => {
    try {
      let enrollments = await Enrollment.find({
        'students.credits': { $gt: 50 },
      })
        .populate({ path: 'students.student' })
        .lean()

      let students = []

      for (let enrollment of enrollments) {
        students = [
          ...students,
          ...[...enrollment.students].filter((s) => s.credits > 50),
        ]
      }

      students = students.map((s) => {
        return { name: s.student.name, credits: s.credits }
      })

      return resolve(students)
    } catch (error) {
      return reject(error)
    }
  })
}

/**
 *
 * @description Servicio para listar los estudiantes y sus creditos
 */
const list = () => {
  return new Promise(async (resolve, reject) => {
    try {
      let enrollments = await Enrollment.find({})
        .populate({ path: 'course' })
        .lean()
        
      let students = await Student.find({}).lean()

      students = JSON.parse(JSON.stringify(students))

      for (let i in students) {
        students[i].courses = []
        for (let enrollment of enrollments) {
          let isFind = enrollment.students.find(
            (s) => s.student == students[i]._id.toString()
          )
          if (isFind)
            students[i].courses.push({
              name: enrollment.course.name,
              credits: isFind.credits,
            })
        }
      }

      return resolve(students)
    } catch (error) {
      return reject(error)
    }
  })
}

module.exports = { create, getFiftyCredits, list }
