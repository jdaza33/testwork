/**
 * @description Servicio de cursos
 */

//Models
const Course = require('../models/course')
const Enrollment = require('../models/enrollment')

/**
 *
 * @description Servicio para crear un curso
 * @param {Object} data
 */
const create = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      const course = await Course.create(data)

      return resolve(course)
    } catch (error) {
      return reject(error)
    }
  })
}

/**
 *
 * @description Servicio para obtener el curso con más estudiantes
 */
const getMoreStudents = () => {
  return new Promise(async (resolve, reject) => {
    try {
      /**
       * Usamos aggregate para usar la funcion size en el arreglo de estudiantes
       */
      let courses = await Enrollment.aggregate([
        {
          $project: {
            sizeStudents: { $size: '$students' },
            courseId: { $toObjectId: '$course' },
            course: 1,
            students: 1,
            _id: 1,
          },
        },
        {
          $sort: { sizeStudents: -1 },
        },
        {
          $limit: 1,
        },
        {
          $lookup: {
            from: 'courses',
            localField: 'courseId',
            foreignField: '_id',
            as: 'course',
          },
        },
        {
          $unwind: {
            path: '$course',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $project: {
            course: 1,
            students: 1,
            _id: 1,
          },
        },
      ])

      courses = courses.length > 0 ? courses[0] : null

      if (courses)
        courses = {
          _id: courses.course._id,
          name: courses.course.name,
          students: courses.students,
        }

      return resolve(courses)
    } catch (error) {
      return reject(error)
    }
  })
}

/**
 *
 * @description Servicio para listar los cursos, sus estudiantes y creditos
 */
const list = () => {
  return new Promise(async (resolve, reject) => {
    try {
      let enrollments = await Enrollment.find({})
        .populate({ path: 'students.student' })
        .lean()

      let courses = await Course.find({}).lean()

      courses = JSON.parse(JSON.stringify(courses))

      for (let i in courses) {
        courses[i].students = []
        let isFind = enrollments.find(
          (e) => e.course == courses[i]._id.toString()
        )
        if (isFind) {
          for (let student of isFind.students)
            courses[i].students.push({
              name: student.student.name,
              credits: student.credits,
            })
        }
      }

      return resolve(courses)
    } catch (error) {
      return reject(error)
    }
  })
}

module.exports = { create, getMoreStudents, list }
