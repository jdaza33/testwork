/**
 * @description Test login
 */

//Modules
const request = require('supertest')
const expect = require('chai').expect

describe('Login - /users/login', function () {
  it('Tendra éxito si los datos son correctos', function (done) {
    request('http://localhost:3001')
      .post('/api/users/login')
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({ email: 'jose@mail.com', password: 'jose1234' })
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(function (response) {
        expect(response.body).not.to.be.empty
        expect(response.body).to.be.an('object')
      })
      .end(done)
  }, 30000)
})
